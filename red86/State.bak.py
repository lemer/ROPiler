from Value import *
from Graph import *
from Node import *

class State:
    # if a register's initial state is None, means the value it had before starting.
    def __init__(self, graph = Graph(), argmem = {}, regass = {}, prev_state = None):
        self.__mem = argmem.copy()
        self.assign = regass
        self.prev_state = prev_state
        self.remaining_graph = graph

    def copy(self, with_graph = None, with_prev_state = None):
        if with_graph == None:
            with_graph = self.remaining_graph.copy()
        ret = State(argmem = self.__mem.copy(), regass = self.assign.copy(), prev_state = with_prev_state, graph = with_graph)
        return ret

    def updateAssign(self, reg, ass_to):
        self.assign[reg] = ass_to

    def nextState(self, action, new_assign = {}):
        next_state = action.apply(self)
        new_ass = self.assign.copy().update(new_assign)
        new_graph = None
        for root in self.remaining_graph.getRoots():
            ass_root_state = root.state.reassigned(new_ass)
            if ass_root_state.mem_equals(next_state):
                new_graph = self.remaining_graph.withoutNode(root)
        if new_graph == None:
            new_graph = self.remaining_graph.copy()
        next_state.assign = new_ass
        next_state.prev_state = self
        next_state.remaining_graph = new_graph


    def getValueAt(self, location):
        if isinstance(location, Value):
            return location
        if not location in self.__mem:
            return Value.atLocation(location)
        return self.__mem[location]

    def equivalence(self, other):
        final_eq = {}
        for l1, v1 in self.__mem.items():
            for l2, v2 in other.__mem.items():
                eq = l1.equivalence(l2)
                if eq != None:
                    eq2 = v1.equivalence(v2)
                    for r, e in eq.items():
                        if r in eq2 and e != eq2[r]:
                            return None
                    eq.update(eq2)
                    for r, e in eq.items():
                        if r in final_eq and e != final_eq[r]:
                            return None
                    final_eq.update(eq)
        return final_eq

    def reassigned(self, reass):
        new_mem = {}
#        print("")
#        print(self.__mem)
        for l, v in self.__mem.items():
#            print("t",l, v, l.reassigned(reass), v.reassigned(reass))
#            print("t",l, v, l.reassigned(reass), v.reassigned(reass))
#            print(l.reassigned(reass) in new_mem, new_mem)
            new_mem[l.reassigned(reass)] = v.reassigned(reass)
#        print(new_mem)
        return State(argmem = new_mem)

    def setValueAt(self, location, new_value):
        if isinstance(new_value, Location):
            raise ValueError("ERROR: cannot set value to be a Location. Must be a value")
        self.__mem[location] = new_value

    def __repr__(self):
        s = "<State : "
        for l, v in self.__mem.items():
            s += (""+str(l)+" : " + str(v)) + ", "
        return s + ">"

    def describe(self):
        print("State:")
        print("\tMemory:\n\t\t"+str(self.__mem))
        print("\tAssignments:\n\t\t"+str(self.assign))
        print("\tPreviousState:\n\t\t"+str(self.prev_state))
        print("\tGraph:")
        s = ""
        for n in self.remaining_graph.getNodes():
            s += str(n)
        print("\n\t\t"+s)


    def mem_equals(self, other):
        return self.__mem == other.__mem

